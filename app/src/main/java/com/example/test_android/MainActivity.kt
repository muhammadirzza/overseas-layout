package com.example.test_android

import androidx.appcompat.app.AppCompatActivity
import android.R.layout.simple_list_item_1
import android.widget.ArrayAdapter
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_sheet.*

class MainActivity : AppCompatActivity() {
    val username = arrayOf<String>("Wildan Syafaat","Sumiati","Abdul Gani","Jumirah","Suryati")

    val datetime = arrayOf<String>(
        "Apr 19, 2021 12:14:31 PM",
        "Apr 19, 2021 12:14:31 PM",
        "Apr 19, 2021 12:14:31 PM",
        "Apr 19, 2021 12:14:31 PM",
        "Apr 19, 2021 12:14:31 PM"
    )

    val imageId = arrayOf<Int>(
        R.drawable.ic_arrow_right,R.drawable.ic_arrow_right,R.drawable.ic_arrow_right,
        R.drawable.ic_arrow_right,R.drawable.ic_arrow_right
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        BottomSheetBehavior.from(sheet).apply {
            peekHeight=800
            this.state=BottomSheetBehavior.STATE_COLLAPSED
        }

        val myListAdapter = MyListAdapter(this,username,datetime,imageId)

        history_listview.adapter = myListAdapter
    }
}