package com.example.test_android

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.*

class MyListAdapter(private val context: Activity, private val username: Array<String>, private val datetime: Array<String>, private val imgid: Array<Int>)
    : ArrayAdapter<String>(context, R.layout.history_listview, username) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.history_listview, null, true)

        val titleText = rowView.findViewById(R.id.text_list_username) as TextView
        val imageView = rowView.findViewById(R.id.icon) as ImageView
        val subtitleText = rowView.findViewById(R.id.text_list_datetime) as TextView

        titleText.text = username[position]
        imageView.setImageResource(imgid[position])
        subtitleText.text = datetime[position]

        return rowView
    }
}